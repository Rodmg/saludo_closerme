var Aq = require("aquilaLib").Aq;
var addressParser = require("aquilaLib").addressParser;
var process = require("child_process");

var file = "/boot/saludo.mp3"

var playing = false;
var callbackSubscribed = false;

Aq.manager.on("ready", function()
		{
			Aq.manager.protocol.on("event", function(packet)
			{
				console.log("Got Event ", packet.message.command[0]," from ", addressParser.toString(packet.srcAddr));
			});

			Aq.manager.on("deviceAdded", function()
			{
				console.log("Device Added");
				if(!callbackSubscribed && Aq("mx.makerlab.movement")[0])
				{
					callbackSubscribed = true;
					Aq("mx.makerlab.movement").on("Hay movimiento", function()
					{
						console.log("Movimiento detectado");
						if(playing == false)
						{
							playing = true;
							var cmd = 'mpg321 ' + file;		
							var player = process.exec(cmd, function (error, stdout, stderr)
							{
								if (error) {
								 console.log(error.stack);
								 console.log('Error code: '+error.code);
								 console.log('Signal received: '+error.signal);
								}
							});

							player.on('exit', function (code)
							{
								playing = false;
							});
						}
					});
				}
			});

			Aq.manager.on("deviceRemoved", function()
			{
				console.log("Device Removed");
			});


			console.log("Protocol ready.");
			console.log("Getting devices...");
			Aq.update(function()
			{
				console.log("Devices ready.");
			});
		});
