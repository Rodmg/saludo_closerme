# Saludo Closerme

## Instalación

* Instalar nodejs
* Actualizar sistema (Hay un bug con el kernel que viene por defecto en el RPi. Se actualizó al 3.12.26+ y parece estar funcionando todo bien)
		```sudo apt-get update && sudo apt-get upgrade```
* Instalar mpg321
		```sudo apt-get install mpg321 git```
* Instalar dependencias, dentro de esta carpeta hacer:
		```npm install```
* Instalar forever:
		```sudo npm install forever -g```
* Poner volumen al máximo
		```amixer  sset PCM,0 100%```
* Forzar salida jack
		```amixer cset numid=3 1```

* Auto Login y startup:

		Editar /etc/inittab y 
		Scroll down to:
		1:2345:respawn:/sbin/getty 115200 tty1

		and change to
		#1:2345:respawn:/sbin/getty 115200 tty1

		Under that line add:
		1:2345:respawn:/bin/login -f pi tty1 </dev/tty1 >/dev/tty1 2>&1

# Editar .bashrc y añadir al final:
if [ $(tty) == /dev/tty1 ]; then
~/saludo_closerme/start.sh
fi
